<?php

namespace Edipresse\Admin;

use TCG\Voyager\VoyagerServiceProvider;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{
    public function register()
    {
        /**
         * Register the voyager package
         */
        $this->app->register(VoyagerServiceProvider::class);

        if ($this->app->runningInConsole()) {
            $this->registerConsoleCommands();
        }
    }

    /**
     * Register the commands accessible from the Console.
     */
    private function registerConsoleCommands()
    {
        $this->commands(Commands\InstallVoyagerCommand::class);
    }
}
