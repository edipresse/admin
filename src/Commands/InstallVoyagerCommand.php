<?php

namespace Edipresse\Admin\Commands;

use TCG\Voyager\Commands\InstallCommand;

class InstallVoyagerCommand extends InstallCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'admin:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the Edipresse Admin package';
}
