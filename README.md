**Laravel docs**

https://laravel.com/docs

**Voyager docs**

https://laravelvoyager.com/docs/0.11

**Admin panel docs**

### Installation

First you need to install a Laravel Framework.

```sh
$ composer create-project --prefer-dist laravel/laravel blog
$ cd blog
```

Then you need to install a Admin Panel

```sh
$ composer require edipresse/admin
```

Next make sure to create a new database and add your database credentials to your .env file:

```php
DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
```

Next, add the Admin panel service provider to the config/app.php file in the providers array:

```php
'providers' => [
    // Laravel Framework Service Providers...
    //...

    // Package Service Providers
    Edipresse\Admin\AdminServiceProvider::class,
    // ...

    // Application Service Providers
    // ...
],
```
Finally, we can install Admin panel with dummy data. 

```sh
$ php artisan admin:install --with-dummy
```